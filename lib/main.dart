import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget{
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

String englishTranslate = "Hello Flutter";
String spanishTranslate = "Hola Flutter";
String SloveniaTranslate = "Pozdravljen Flutter";
String SwedenTranslate = "Hej Flutter";

class _MyStatefulWidgetState extends State<HelloFlutterApp>{
  String displayText = englishTranslate;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      //Scaffold Widget
      home: Scaffold(
        appBar: AppBar(
          title: Text("Oh Hi Mark!"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(onPressed: () {
              setState(() {
                displayText = displayText == englishTranslate? spanishTranslate : englishTranslate;});
            }, icon: Icon(Icons.translate)), 
            IconButton(onPressed: () {
              setState(() {
                displayText = displayText == englishTranslate? SloveniaTranslate : englishTranslate;});
            }, icon: Icon(Icons.add_alert_sharp)),
            IconButton(onPressed: () {
              setState(() {
                displayText = displayText == englishTranslate? SwedenTranslate : englishTranslate;});
            }, icon: Icon(Icons.waving_hand_sharp)),
          ],
        ),
        body: Center(
          child: Text(
            displayText,
            style:
                TextStyle(fontSize: 48, backgroundColor: Colors.pink.shade300),
          ),
        ),
        backgroundColor: Colors.blueGrey,
              bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            label: 'Business',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.school),
            label: 'School',
          ),
        ],
        selectedItemColor: Colors.amber[800],
      ),
      ),
    );
  }

}
// class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       //Scaffold Widget
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Oh Hi Mark!"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(onPressed: () {}, icon: Icon(Icons.refresh))
//           ],
//         ),
//         body: Center(
//           child: Text(
//             "Hello Flutter",
//             style:
//                 TextStyle(fontSize: 48, backgroundColor: Colors.pink.shade300),
//           ),
//         ),
//       //   backgroundColor: Colors.blueGrey,
//       //         bottomNavigationBar: BottomNavigationBar(
//       //   items: const <BottomNavigationBarItem>[
//       //     BottomNavigationBarItem(
//       //       icon: Icon(Icons.headphones_battery_outlined),
//       //       label: 'Home',
//       //     ),
//       //     BottomNavigationBarItem(
//       //       icon: Icon(Icons.bedtime),
//       //       label: 'Business',
//       //     ),
//       //     BottomNavigationBarItem(
//       //       icon: Icon(Icons.access_alarm_sharp),
//       //       label: 'School',
//       //     ),
//       //   ],
//       //   selectedItemColor: Colors.amber[800],
//       // ),
//       ),
//     );
//   }
// }
